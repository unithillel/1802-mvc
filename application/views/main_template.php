<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div>
        <ul>
            <li><a href="/">Home page</a></li>
            <li><a href="/Main/contacts">Contacts page</a></li>
            <li><a href="/Portfolio">Portfolio</a></li>
        </ul>
    </div>
    <hr>
    <div>
        <?php include_once $contentView;?>
    </div>
    <hr>

    <div>
        <p>Hillel MVC(c)2018</p>
    </div>
</body>
</html>